import React from 'react'
import { Iterable } from 'immutable'

/**
 * Convert immutable to javascript object
 * 
 * @param {Function} WrappedComponent 
 */
const PropsToJs = WrappedComponent => WrappedComponentProps => {
  const KEY = 0
  const VALUE = 1

  const propsJS = Object.entries(WrappedComponentProps).reduce(
    (newProps, wrappedComponentProp) => {
      newProps[wrappedComponentProp[KEY]] = Iterable.isIterable(
        wrappedComponentProp[VALUE]
      )
        ? wrappedComponentProp[VALUE].toJS()
        : wrappedComponentProp[VALUE]
      return newProps
    },
    {}
  )

  return <WrappedComponent {...propsJS} />
} 

export default PropsToJs