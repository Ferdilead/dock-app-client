import React from 'react'
import PokemonCollectionsContainer from '@/containers/PokemonCollectionsContainer'
import { FeedContainer } from './styles'

/**
 * Collections
 */
const Collections = () => (
  <FeedContainer>
    <PokemonCollectionsContainer />
  </FeedContainer>
)

export default Collections