import styled from 'styled-components'

export const FeedContainer = styled.div`
  display: block;
  margin: 0 auto;
  overflow: hidden;
`