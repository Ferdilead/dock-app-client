import React, { Fragment, Suspense } from 'react'
import PokemonDetailContainer from '@containers/PokemonDetailContainer' 
import { FeedDetailContainer } from './styles'

/**
 * Detail Page
 */
const DetailPage = () => (
  <FeedDetailContainer>
    <PokemonDetailContainer />
  </FeedDetailContainer>
)

export default DetailPage