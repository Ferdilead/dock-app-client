import React from 'react'
import PokemonListContainer from '@/containers/PokemonListContainer'
import { FeedContainer } from './styles'

/**
 * Homepage
 */
const HomePage = () => (
  <FeedContainer>
    <PokemonListContainer />
  </FeedContainer>
)

export default HomePage