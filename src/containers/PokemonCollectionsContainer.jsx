import React, { Component } from 'react'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import PropsToJs from '@hoc/propsToJs'
import { getAllCollectionsStart, deleteCollectionsStart } from '@store/collections/actions'
import { selectCollections } from '@store/collections/selector'
import PokemonList from '@components/pokemon/lists'

/**
 * Pokemon detail container
 */
class PokemonCollectionsContainer extends Component {
  componentDidMount() {
    const { fetchCollections } = this.props
    
    fetchCollections()
  }

  render() {
    const { collections, deleteCollection } = this.props

    console.log('COLLECTIONS', collections)
    return(
      <PokemonList pokemons={collections.storage} isCollections={true} deleteCollection={deleteCollection} />
    )
  }
}

const mapStateToProps = createStructuredSelector({
  collections: selectCollections
})

const mapDispatchToProps = dispatch => ({
  fetchCollections: () => dispatch(getAllCollectionsStart()),
  deleteCollection: (pokemon) => dispatch(deleteCollectionsStart(pokemon))
})

export default connect(mapStateToProps, mapDispatchToProps)(PropsToJs(PokemonCollectionsContainer))
