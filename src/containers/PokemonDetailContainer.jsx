import React, { Component } from 'react'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import { selectDetailPokemon } from '@store/pokemon-detail/selector'
import { withRouter } from 'react-router-dom'
import PropsToJs from '@hoc/propsToJs'
import PokemonDetail from '@components/pokemon/detail'
import { requestPost } from '@store/pokemon-detail/actions'
import { getCollectionsStart, postCollectionsStart } from '@store/collections/actions'
import { selectCollections } from '@store/collections/selector'
import { selectCurrentUser } from '@store/user/selector'
import { createPokemonDocument, getPokemonDocument } from '@/firebase/firebase.util'

/**
 * Pokemon detail container
 */
class PokemonDetailContainer extends Component {
  componentDidMount() {
    const { fetchData, match: {params} } = this.props
    
    fetchData(params.pokemonId)
  }

  render() {
    const { pokemon: {pokemonDetail, pokemonSpecies}, collections, user, postCollections } = this.props

    return(
      <PokemonDetail 
        pokemon={pokemonDetail} 
        species={pokemonSpecies} 
        collections={collections} 
        user={user}
        postToCollections={postCollections}
        />
    )
  }
}

const mapStateToProps = createStructuredSelector({
  pokemon: selectDetailPokemon,
  collections: selectCollections,
  user: selectCurrentUser
})

const mapDispatchToProps = dispatch => ({
  fetchData: params => dispatch(requestPost(params)),
  fetchCollections: (userAuth, pokemon) => dispatch(getCollectionsStart(userAuth, pokemon)),
  postCollections: (nickname) => dispatch(postCollectionsStart(nickname))
})

export default connect(mapStateToProps, mapDispatchToProps)(PropsToJs(withRouter(PokemonDetailContainer)))
