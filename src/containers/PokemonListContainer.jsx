import React, { Component } from 'react'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import { selectPokemons } from '@store/pokemon/selector'
import PropsToJs from '@hoc/propsToJs'
import { requestPost } from '@store/pokemon/actions'
import PokemonList from '@components/pokemon/lists'
import InifiniteScroll from '@components/infinite-scroll'

/**
 * Pokemon list container
 */
class PokemonListContainer extends Component {
  componentDidMount() {
    const { fetchData } = this.props
    
    // Fetch pokemon data
    fetchData()
  }

  loadMore = () => {
    const { fetchData, pokemons: {metadata} } = this.props
    
    if(metadata.lastPage) return false

    fetchData(metadata.next)
  }

  render() {
    const { isFetching, items } = this.props.pokemons

    return(
      <React.Fragment>
        <InifiniteScroll onEndChange={this.loadMore} fallback={isFetching}>
          <PokemonList pokemons={items} /> 
        </InifiniteScroll>
      </React.Fragment>
    )
  }
}

const mapStateToProps = createStructuredSelector({
  pokemons: selectPokemons
})

const mapDispatchToProps = dispatch => ({
  fetchData: params => dispatch(requestPost(params))
})

export default connect(mapStateToProps, mapDispatchToProps)(PropsToJs(PokemonListContainer))