const API_BASE_URL = 'https://pokeapi.co/api/v2'

export default {
  API_POKEMON_URL: API_BASE_URL.concat('/pokemon'),
  API_POKEMON_SPECIES_URL: API_BASE_URL.concat('/pokemon-species/')
}