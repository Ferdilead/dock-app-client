import Url from '@helpers/urls' 

/**
 * Fetch pokemon data
 *
 * @param
 */
export const fetchData = async url => {
  try {
    const dataResponse = await fetch(url)
    const dataCollection = await dataResponse.json()
    
    return dataCollection
  }catch (e) {
    console.error('FAILED_TO_FETCH_POKEMON', e)
  }
}