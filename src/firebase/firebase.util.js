import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/database'
import 'firebase/auth'

const firebaseConfig = {
  apiKey: "AIzaSyCjd8C_1qakj9zq9Q3-H6TSe2qBCCJvfFY",
  authDomain: "dock-client-app.firebaseapp.com",
  databaseURL: "https://dock-client-app.firebaseio.com",
  projectId: "dock-client-app",
  storageBucket: "dock-client-app.appspot.com",
  messagingSenderId: "278206899429",
  appId: "1:278206899429:web:b13de46b48e004b82f600c",
  measurementId: "G-EBEV0PFJ3Q"
}

/**
 * Create user for authentication
 * 
 * @param {Object} userAuth 
 * @param {Object} additionalData 
 */
export const createUserProfileDocument = async (userAuth, additionalData) => {
  if(!userAuth) return

  const userRef = firestore.doc(`users/${userAuth.uid}`)
  const snapShot = await userRef.get()

  if(!snapShot.exists) {
    const { displayName, email } = userAuth
    const createdAt = new Date()

    try {
      await userRef.set({
        displayName, 
        email,
        createdAt,
        ...additionalData
      })
    } catch(error) {
      console.error('error creating user', error.message)
    }
  }

  return userRef
}

/**
 * Get pokemon document
 * 
 * @param {Object} userAuth 
 * @param {Object} pokemon 
 */
export const getPokemonDocument = async (userAuth, pokemon) => {
  const dataRef = firestore.collection('c_pokemons')
  let snapShot = await dataRef.where('userId', '==', userAuth.uid)

  // debugger
  if(pokemon)
    snapShot = await snapShot.where('id', '==', pokemon.id)
  
  return snapShot
}

/**
 * Create pokemon document
 * 
 * @param {Object} userAuth 
 * @param {Object} additionalData 
 */
export const createPokemonDocument = async (userAuth, additionalData) => {
  if(!userAuth) return

  const dataRef = firestore.collection('c_pokemons').doc()
  const snapShot = await getPokemonDocument(userAuth, additionalData)

  if(!snapShot.empty) {
    const createdAt = new Date()

    try {
      await dataRef.set({
        createdAt,
        userId: userAuth.uid,
        ...additionalData,
      })
    } catch(error) {
      console.error('ERROR_SAVE_POKEMON_TO_COLLECTION', error.message)
    }
  }
  else
    console.warn('POKEMON HAS EXISTS!')

  return dataRef
}

// Init the firebase config
firebase.initializeApp(firebaseConfig)

/**
 * Get current user authenticated
 */
export const getCurrentUser = () => {
  return new Promise((resolve, reject) => {
    const unsubscribe = auth.onAuthStateChanged(userAuth => {
      unsubscribe()

      resolve(userAuth)
    }, reject)
  })
}

export const auth = firebase.auth()
export const firestore = firebase.firestore()
export const db = firebase.database()

export const googleProvider = new firebase.auth.GoogleAuthProvider()
googleProvider.setCustomParameters({ prompt: 'select_account' })

export const SignInWithGoogle = () => auth.signInWithPopup(googleProvider)

export default firebase