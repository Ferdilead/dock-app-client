/**
 * Test for an empty JavaScript object?
 * @param {Object} obj 
 */
export const isEmpty = obj => {
  return Object.entries(obj).length === 0 && obj.constructor === Object
}