/**
 * Convert pound to kilogram
 * 
 * @param {Number} lb 
 */
export const lbToKg = lb => {
  const count = lb * .45
  const result = count.toString().split('').splice(0, 4).join('')

  return `${result}kg`
} 

/**
 * Convert inches to centimeter
 * 
 * @param {Number} inches 
 */
export const inchesToCm = inches => {
  const PI = 2.54
  
  return `${inches * PI}cm` 
}