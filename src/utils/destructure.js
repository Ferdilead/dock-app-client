/**
  * Destructuring an object and select key by params
  * 
  * @param {Array} arguments
  * @return {Object}
  */
export function destructure() {
  let args        = Array.prototype.slice.call(arguments, 0), 
      data        = [].concat(args).slice(args.length - 1, args.length)[0],
      keysArr     = args.slice(0, args.length - 1),
      filtered;
      
  if (typeof data !== 'object') return {};
  
  filtered = Object.keys(data)
    .filter(key => keysArr.includes(key))
    .reduce(function(obj, key) {
      obj[key] = data[key];
      return obj;
    }, {});

  return filtered;
}