import React from 'react'
import { HeaderContainer, Wrapper, HomeLink, Logo } from './styles'
import NavigationLink from './navigation'
import IconUrl from '@/assets/images/logo.png'

const Header = () => (
  <HeaderContainer>
    <Wrapper>
      <HomeLink to="/">
        <Logo src={IconUrl}></Logo>
      </HomeLink>

      <NavigationLink />
    </Wrapper>
  </HeaderContainer>
)

export default Header