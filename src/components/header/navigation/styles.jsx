import { Link } from 'react-router-dom'
import styled from 'styled-components'

export const Nav = styled.nav`
  display: flex;
  align-items: center;
`

export const AuthLink = styled.div`
  font-size: 14px;
  color: #FFFFFF;
  cursor: pointer;
  font-weight: 700;
`

export const MyCollection = styled(Link)`
  text-decoration: none;
  font-size: 14px;
  color: #FFFFFF;
  cursor: pointer;
  font-weight: 700;
`