import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import { Nav, AuthLink, MyCollection } from './styles'
import { setCurrentUser } from '@store/user/actions'
import { selectCurrentUser } from '@store/user/selector'
import { googleSignInStart } from '@store/user/actions'
import PropsToJs from '@/hoc/propsToJs'

/**
 * Navigation link
 */
const NavigationLink = props => {
  const { user: { currentUser }, googleSignInStart } = props

  const handleClick = () => {
    googleSignInStart()
  }

  return(
    <Nav>
      {!currentUser.id ? (
          <AuthLink onClick={handleClick}>Sign In</AuthLink>
        ) : (
          <MyCollection to="/my-collection">Collections</MyCollection>
        )
      }
    </Nav>
  )
}

const mapStateToProps = createStructuredSelector({
  user: selectCurrentUser
})

const mapDispacthToProps = dispatch => ({
  setCurrentUser: user => dispatch(setCurrentUser(user)),
  googleSignInStart: () => dispatch(googleSignInStart())
})

export default connect(mapStateToProps, mapDispacthToProps)(PropsToJs(NavigationLink))