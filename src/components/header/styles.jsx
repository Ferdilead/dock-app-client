import { Link } from 'react-router-dom'
import styled from 'styled-components'

export const HeaderContainer = styled.div`
  background-color: #ef5350;
  box-shadow: 0 2px 2px rgba(0,0,0,.2);
  position: sticky;
  z-index: 100;
  top: 0;
  height: 3.125rem;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
  user-select: none;
  padding: 0 25px;
`

export const Wrapper = styled.div`
  max-width: 1120px;
  height: 100%;
  margin: 0 auto;
  display: flex;
  align-items: center;
  justify-content: space-between;
`

export const HomeLink = styled(Link)`
  height: 100%;
  display: flex;
  align-items: center;
  color: #f3f3f3;
  // text-decoration: none;
`

export const Logo = styled.img`
  height: 35px;
  min-width: 3em;
  padding: 2px;  
`