import React, { useEffect, useState, memo } from 'react'
import { CardItem } from './styles'
import ChildItemWrapper from './ChildItemWrapper'
import ChildItem from './ChildItem'

/**
 * Pokemon Item
 * 
 * @param {String} url
 * @param {String} name 
 */
const PokemonItem = props => (
  <CardItem>
    <ChildItem {...props} />
  </CardItem>
)

export default ChildItemWrapper(PokemonItem)