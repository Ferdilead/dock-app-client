import { Link } from 'react-router-dom'
import styled from 'styled-components'
import Card from '@micro-ui/components/card'

export const CardItem = styled(Card)`
  width: 100%;
  height: 120px;
  position: relative;
  background-color: #f7a179;
  border-radius: 10px;
  padding: 5px 10px;
  display: flex;
  flex-direction: row-reverse;

  &:nth-child(2n) {
    background-color: #f387ff;
  }

  &:nth-child(3n) {
    background-color: #f060b9;
  }

  &:nth-child(5n) {
    background-color: #afe5ed;
  }

  &:nth-child(6n) {
    background-color: #8cb9bf;
  }
`

export const Image = styled.img`
  width: auto;
  height: auto;
  position: absolute;
  max-width: 100%;
  max-height: 100%;
  left: 0;
  top: 0;
  right: 0;
  bottom: 0;
  margin: auto;
`

export const ItemLink = styled(Link)`
  position: absolute;
  width: 100%;
  height: 100%;
  display: block;
  z-index: 2;
`

export const ItemImage = styled.div`
  width: 30%;
  height: 100%;
  position: relative;
  overflow: hidden;
  flex-grow: 1;
  background-color: rgba(255, 255, 255, .8);
  position: absolute;
  right: 0;
  top: 0;
  bottom: 0;
  border-top-left-radius: 60px;
  border-bottom-left-radius: 60px;
`

export const Detail = styled.div`
  width: 60%;
  height: 100%;
  text-align: center;
  flex-grow: 2;
  display: flex;
  flex-direction: column;
  justify-content: center;
  position: absolute;
  left: 10px;
  top: 0;
`

export const TitleDetail = styled.div`
  margin-bottom: 18px;
  font-weight: 300;
  font-size: 16px;
  display: flex;
  align-items: baseline;
`

export const DetailName = styled.div`
  text-transform: capitalize;
  margin-left: 20px;
`

export const DetailNumber = styled.div`

`

export const TypeName = styled.div`
  width: 100%;  
  display: flex;
`

export const BadgeType = styled.div`
  width: 100%;
  background-color: transparent;
  border: .8px solid #333333;
  border-radius: 5px;
  padding: 1px 10px;
  text-transform: uppercase;
  font-size: 12px;
  font-weight: 300;

  &:first-child {
    margin-right: 5px;
  }
`

export const DeleteButton = styled.div`
  position: absolute;
  width: auto;
  height: auto;
  border-radius: 50px;
  display: block;
  background-color: red;
  color: white;
  z-index: 3;
  padding: 0 5px;
  font-size: 12px;
  cursor: pointer;
`

export const Nickname = styled.span`
  font-size: 12px;
  font-style: italic;
  font-weight: 700;
  margin-left: 10px;
`