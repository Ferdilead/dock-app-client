import React, { Component } from 'react'

/**
 * Child Item Wrapper
 * 
 * @param {Fucntion} WrapperComponent 
 */
const ChildItemWrapper = WrapperComponent  => { 
  return class extends Component {
    abortController = new AbortController()

    constructor(props) {
      super(props)

      this.state = {
        loaded: false,
        newProps: {}
      }
    }

    componentDidMount() {
      const { name, isCollections } = this.props
      
      const fetchPokemon = async () => {
        try {
          const dataResponse = await fetch(`https://pokeapi.co/api/v2/pokemon/${name}`, { signal: this.abortController.signal })
          const dataCollection = await dataResponse.json()
          
          this.setState({
            loaded: true,
            newProps: dataCollection 
          })

          return
        } catch(e) {}
      }

      if(!isCollections) fetchPokemon()
    }

    UNSAFE_componentWillMount() {
      // this.abortController.abort()
    }

    render() {
      const { isCollections } = this.props
      const { newProps, loaded } = this.state
      const childProps = (isCollections) ? this.props : newProps
      
      return <>{(loaded || isCollections) && <WrapperComponent {...childProps}></WrapperComponent>}</>
    }
  }
}

export default ChildItemWrapper