import React from 'react'
import { formatNumber } from '@utils/number'
import { 
  Image, 
  ItemLink, 
  ItemImage, 
  Detail, 
  TitleDetail, 
  DetailNumber, 
  DetailName, 
  TypeName, 
  BadgeType,
  DeleteButton,
  Nickname 
} from './styles' 
/**
 * Child item
 */
const ChildItem = ({ name, id, isCollections, sprites, types, nickname, deleteCollection }) => (
  <React.Fragment>
    {name && <ItemLink to={`/poke/${name}`} />}

    {isCollections && <DeleteButton onClick={() => deleteCollection({id})}>X</DeleteButton>}

    <ItemImage>
      <Image src={sprites.front_default} alt={name} />
    </ItemImage>
    
    <Detail>
      <TitleDetail>
        <DetailNumber>#{formatNumber(id, 3)}</DetailNumber>
        <DetailName>{name}</DetailName>

        {nickname && <Nickname>as {nickname}</Nickname>}  
      </TitleDetail>

      <TypeName>
        {types.map((item, id) => (
          <BadgeType key={id}>{item.type.name}</BadgeType>
        ))}
      </TypeName>
    </Detail>
  </React.Fragment>
)

export default ChildItem