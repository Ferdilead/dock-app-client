import React from 'react'
import { List } from './styles'
import PokemonItem from '@components/pokemon/item'

const inifiniteScrollRef = React.createRef()

/**
 * List of pokemon
 * 
 * @param {Array} pokemons 
 */
const PokemonList = ({ pokemons, ...otherProps }) => {
  return(
    <List>
      {pokemons.map((item, id) => 
        <PokemonItem key={id} {...otherProps} {...item} />
      )}
    </List>
  )
}

export default PokemonList