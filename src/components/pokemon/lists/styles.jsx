import styled from 'styled-components'

export const List = styled.div`
  max-width: 745px;
  margin: 0 auto;
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-gap: 10px;
  padding: 0 10px;
  
  @media(max-width: 530px) {
    grid-template-columns: repeat(1, 1fr);
  }

  @media(max-width: 440px) {
    grid-template-columns: repeat(1, 1fr);
  }
`