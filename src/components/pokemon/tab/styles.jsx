import styled from 'styled-components'

export const TabContainer = styled.div`
  width: 100%;
`

export const TabList = styled.div`
  display: flex;
`

export const TabItem = styled.div`
  width: 25%;
  font-size: 12px;
  padding: 5px 10px 15px;
  position: relative;
  cursor: pointer;
  text-align: center;

  ${({active}) => active &&
    `
      font-weight: 800;

      &:after {
        content: '';
        position: absolute;
        left: 0;
        bottom: 0;
        width: 100%;
        height: 2px;
        background-color: #333333;
      }
    `
  }
`