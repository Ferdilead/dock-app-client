import React, { useContext } from 'react'
import useConstant from 'use-constant'
import { TabContainer, TabList, TabItem } from '@components/pokemon/tab/styles'
import { DetailPanelContext, TabPanelContext } from  '@components/pokemon/detail/detail-panel'

/**
 * Tab item
 * 
 * @param {Object} props
 */
const TabItems = props => {
  const [panels, setActive] = useContext(DetailPanelContext)
  const [tabs] = useContext(TabPanelContext)

  // just creating a value exactly once
  const tabIndex = useConstant(() => {
    const currTabIndex = tabs.index
    tabs.index += 1

    return currTabIndex
  })

  const handleClick = () => setActive({ activeIndex: tabIndex})
  const isActive = tabIndex === panels.activeIndex

  return(
    <TabItem onClick={handleClick} active={isActive}>{props.children}</TabItem>
  )
}

/**
 * Tab menu of detail pokemon
 */
export const TabNavigation = () => {
  const tabs = ['About', 'Stats', 'Moves']

  return(
    <TabContainer>
      <TabList>
        {tabs.map((item, id) => (
            <TabItems key={id}>{item}</TabItems>
          ))
        }
      </TabList>
    </TabContainer>
  )
}