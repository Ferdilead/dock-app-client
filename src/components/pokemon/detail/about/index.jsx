import React from 'react'
import { Text } from '@micro-ui/components/text'
import { Label } from '@micro-ui/components/label'
import { Column } from './styles'
import Abilities from '@components/pokemon/detail/abilities'
import TypeList from '@components/pokemon/detail/types'
import { lbToKg, inchesToCm } from '@utils/sizeConverter'

/**
 * Detail of selected pokemon
 * 
 * @param {Object} param 
 */
const About = ({ name, height, weight, abilities, types }) => {
  return(
    <React.Fragment>
      <Column>
        <Label>Name</Label>
        <Text>{name}</Text>
      </Column>

      <Column>
        <Label>Height</Label>
        <Text>
          {height}"
          ({inchesToCm(height)})
        </Text>
      </Column>

      <Column>
        <Label>Weight</Label>
        <Text>
          {weight} lbs
          ({lbToKg(weight)})
        </Text>
      </Column>

      <Column>
        <Label>Abilities</Label>
        <Abilities abilities={abilities} />
      </Column>

      <Column>
        <Label>Types</Label>
        <TypeList types={types} />
      </Column>
    </React.Fragment>
  )
}

export default About