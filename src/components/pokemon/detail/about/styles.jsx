import styled from 'styled-components'

export const Column = styled.div`
  margin-bottom: 10px;

  &:last-child {
    margin-bottom: 0;
  }
`