import React, { Component } from 'react'
import { FeedContainer } from './styles'
import Profile from './profile'

/**
 * Detail of Pokemon
 */
const PokemonDetail = (props) => {
  return(
    <FeedContainer>
      <Profile {...props} />
    </FeedContainer>
  )
}

export default React.memo(PokemonDetail)