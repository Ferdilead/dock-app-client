import React from 'react'
import { Text } from '@micro-ui/components/text'

/**
 * List of abilities
 * 
 * @param {Array} abilities 
 */
const Abilities = ({ abilities }) => (
  <Text>
    {abilities.map(item => item.ability.name).join(', ')}
  </Text>
)

export default Abilities