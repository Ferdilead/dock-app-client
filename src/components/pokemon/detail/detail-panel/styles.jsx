import styled from 'styled-components'

export const DetailContainer = styled.div`
  padding: 15px;
  overflow: hidden;
  width: 100%;
`
export const Content = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  padding: 15px;
`