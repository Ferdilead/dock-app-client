import React, { useContext, createContext, useState } from 'react'
import useConstant from 'use-constant'
import { DetailContainer, Content } from './styles'

/**
 * Detail Panel Context
 */
export const DetailPanelContext = createContext()

/**
 * Tab Panel Context
 */
export const TabPanelContext = createContext()

/**
 * Detail panel context
 * 
 * @param {Object} children 
 */
export const DetailPanel = ({ children }) => {
  const panel = useState({ activeIndex: 0, index: 0 })
  const tab = useState({ tabs: 0, index: 0 }) 

  return(
    <DetailPanelContext.Provider value={panel}>
      <TabPanelContext.Provider value={tab}>
        <DetailContainer>
          {children}
        </DetailContainer>
      </TabPanelContext.Provider>
    </DetailPanelContext.Provider>
  )
}

/**
 * Panel of detail
 * 
 * @param {Object} props
 */
export const Panel = props => {
  const [elements] = useContext(DetailPanelContext)

  // just creating a value exactly once
  const panelIndex = useConstant(() => {
    const currPanelIndex = elements.index
    elements.index += 1

    return currPanelIndex
  })

  return (elements.activeIndex === panelIndex) 
    ? <Content>{props.children}</Content> 
    : null
}