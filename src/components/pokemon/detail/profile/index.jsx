import React, { useState } from 'react'
import About from '@components/pokemon/detail/about'
import Species from '@components/pokemon/detail/species'
import Stats from '@components/pokemon/detail/stats'
import Moves from '@components/pokemon/detail/moves'
import TypeList from '@components/pokemon/detail/types'
import { DetailPanel, Panel } from '@/components/pokemon/detail/detail-panel'
import { TabNavigation } from '@components/pokemon/tab'
import { 
  ProfileContainer, 
  ImageBox, 
  Image,
  HeaderProfile,
  Title,
  RightBox,
  CatchBox,
  CatchButton,
  NicknameField,
  CatchRate 
} from './styles'
import { isEmpty } from '@utils/object'
import PropsToJs from '@/hoc/propsToJs'

/**
 * Profile of selected pokemon
 * 
 * @param {Object} pokemon 
 */
const Profile = ({ pokemon, species, user, collections, postToCollections }) => {
  const { name, sprites, types } = pokemon
  const [nickname, setNickname] = useState('')
  const [caught, setCaught] = useState(false)
  const [catchRate, setCatchRate] = useState()

  const handleChange = e => {
    setNickname(e.target.value)
  }

  const handleCatch = () => {
    const catchRate  = (species.capture_rate * 100 / 255).toFixed(2).concat('%')

    setCatchRate(catchRate)
    setCaught(true)
  }

  return( 
    <ProfileContainer>
      <HeaderProfile>
        <ImageBox>
          {!isEmpty(pokemon) && <Image src={sprites.front_default} alt={name} />}
        </ImageBox>

        <RightBox>
          {!isEmpty(pokemon) && (
            <React.Fragment>
              <Title>{name}</Title>
              <TypeList types={types} />
            </React.Fragment>
          )}
        </RightBox>
      </HeaderProfile>

      <DetailPanel>
        <TabNavigation />

        <Panel>
          {!isEmpty(pokemon) && <About {...pokemon} />}
          
          {!isEmpty(species) && <Species {...species} />}

          {(!(collections.caught)) &&
            <CatchBox>
              {caught ? (
                  <React.Fragment>
                    <CatchButton onClick={() => postToCollections({nickname})}>Save to collections</CatchButton>
                    <CatchRate>{catchRate}</CatchRate>
                    <NicknameField placeholder="Nickname ..." onChange={handleChange}></NicknameField>
                  </React.Fragment>
                ) : (
                  <CatchButton onClick={handleCatch}>Catch Me</CatchButton>
                )
              }
            </CatchBox>
          }
        </Panel>
        <Panel>
          {!isEmpty(pokemon) && <Stats {...pokemon} />}
        </Panel>
        <Panel>
          {!isEmpty(pokemon) && <Moves {...pokemon} />}
        </Panel>
      </DetailPanel>
    </ProfileContainer>
  )
}

export default React.memo(PropsToJs(Profile))