import styled from 'styled-components'

export const ProfileContainer = styled.div`
  width: 100%;
  height: auto;
  display: -ms-flexbox;
  border-radius: 15px;
  display: inline-block;
  overflow: hidden;
  position: relative;
  box-shadow: 1px 1px 2px #dadade;
  float: left;
`

export const HeaderProfile = styled.div`
  width: 100%;
  height: auto;
  position: relative;
  // background-color: rgba(243, 135, 255, .7);
  padding: 10px;
  display: flex;
  align-items: center;
`

export const Title = styled.h2`
  font-size: 24px;
  font-weight: 300;
  text-transform: capitalize;
  margin: 10px 0;
`

export const RightBox = styled.div`
  float: left;
`

export const TopLabel = styled.div`
  // position: absolute;
  font-size: 20px;
  text-transform: capitalize;
  font-weight: bold;
  color: #ffffff;
  top: 35px;
  left: 15px;
`

export const CatchBox = styled.div`
  display: flex;
  align-items: center;
`

export const NicknameField = styled.input`
  height: 30px;
  border-radius: 5px;
  box-shadow: none;
  border: 1px solid #dadade;
  margin-left: 10px;
  padding: 0 10px;
`

export const CatchButton = styled.button`
  width: auto;
  min-width: 80px;
  height: 30px;
  background-color: black;
  font-size: 12px;
  color: white;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 5px;
  cursor: pointer;
  border: none;
`

export const CatchRate = styled.span`
  font-size: 12px;
  font-weight: 700;
  margin: 0 0 0 10px;
`

export const ImageBox = styled.div`
  width: 100px;
  height: 100%;
  overflow: hidden;
  background-color: rgba(255,255,255,.8);
  display: inline-block;
  float: left;
`

export const Image = styled.img`
  width: 100%]px;
  height: auto;
  position: absolute;
  display: block;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
`

export const Detail = styled.div`
  padding: 15px;
  position: relative;
  overflow: hidden;
  width: 100%;
`