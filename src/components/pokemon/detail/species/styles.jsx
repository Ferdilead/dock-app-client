import styled from 'styled-components'

export const SpeciesContainer = styled.div`
  position: relative;
`

export const ColumnGrid = styled.div`
  display: grid;
  grid-template-columns: 100px 70%;
  margin-bottom: 10px;
`