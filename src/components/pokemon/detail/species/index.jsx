import React from 'react'
import { Label } from '@micro-ui/components/label'
import { Text } from '@micro-ui/components/text'
import { SpeciesContainer, ColumnGrid } from './styles'

/**
 * Pecies of pokemon
 * 
 * @param {Object} species
 */
const Species = species => {
  const description = species.flavor_text_entries.find(text => text.language.name === 'en')

  return(
    <React.Fragment>
      {species ? 
        <SpeciesContainer>
          <ColumnGrid>
            <Label>Species</Label>
            <Text>
              {description.flavor_text}
            </Text>
          </ColumnGrid>

          <ColumnGrid>
            <Label>Capture rate</Label>
            <Text>
              {species.capture_rate}
            </Text>
          </ColumnGrid>

          <ColumnGrid>
            <Label>Base Happiness</Label>
            <Text>
              {species.base_happiness}
            </Text>
          </ColumnGrid>

          {/* <ColumnGrid>
            <Label>Evolution Chain</Label>
            <Text>
              It has no eyeballs, so it can’t see. It checks its
              surroundings via the ultrasonic waves it emits
              from its mouth.
            </Text>
          </ColumnGrid> */}
        </SpeciesContainer>
        : null
      }
    </React.Fragment>
  )
}

export default Species