import React from 'react'
import { TypeListBox } from './styles'
import { Badge } from '@micro-ui/components/badge'

/**
 * Types of pokemon
 * 
 * @param {Array} types
 */
const TypeList = ({ types }) => (
  <TypeListBox>
    {types.map((item, id) => (
      <Badge key={id}>{item.type.name}</Badge>
    ))}
  </TypeListBox>
)

export default TypeList