import styled from 'styled-components'

export const TypeListBox = styled.div`
  display: flex;
  flex-wrap: wrap;

  span {
    font-size: 12px;
    padding: 3px 10px;

    &:nth-child(2n) {
      background-color: red;
    }
  }
`
