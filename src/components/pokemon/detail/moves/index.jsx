import React from 'react'
import MoveList from './lists'
import { MoveContainer } from './styles'

/**
 * Move of pokemon
 */
const Move = ({ moves }) => {
  return(
    <MoveContainer>
      <MoveList moves={moves} />
    </MoveContainer>
  )
}

export default Move