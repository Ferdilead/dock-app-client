import React, { useState, useEffect, memo } from 'react'
import { Label } from '@micro-ui/components/label'
import { 
  Item,
  Column,
  Title,
  RangeColumn,
  RangeSlider,
  Range
} from './styles'

/**
 * Move item of pokemon
 * 
 * @param {Object} move
 */
const MoveItem = memo(({ move: { name, url } }) => {
  let formatedMoveName, description
  const [data, setData] = useState({
    isFetching: true,
    move: {}
  })

  useEffect(() => {
    let abortController = new AbortController()

    const fetchData = async url => {

      try {
        const dataResponse = await fetch(url, { signal: abortController.signal })
        const dataCollection = await dataResponse.json()
        
        setData({ 
          isFetching: false,
          move: dataCollection
        })
      }
      catch(e) {
        console.error('FAILED_TO_FETCH_POKEMON_MOVE', e)
      }
    }

    fetchData(url)

    return () => abortController.abort()
  }, [url])

  // Desctructure the state
  const { 
    move: {
      pp, 
      priority, 
      accuracy,
      power, 
      flavor_text_entries
    }, 
    isFetching 
  } = data

  if(!isFetching) {
    formatedMoveName = name.split('-').join(' ')
    description = flavor_text_entries.find(item => item.language.name === 'en').flavor_text
  }

  return(
    <Item>
      <Column>
        <Title>{formatedMoveName}</Title>
        <Label width="auto">{description}</Label>
      </Column>

      <RangeColumn>
        <Range>
          <Label>PP</Label>
          <RangeSlider width={pp} />
        </Range>

        <Range>
          <Label>Power</Label>
          <RangeSlider width={power} />
        </Range>

        <Range>
          <Label>Priority</Label>
          <RangeSlider width={priority} />
        </Range>

        <Range>
          <Label>Accuracy</Label>
          <RangeSlider width={accuracy} />
        </Range>
      </RangeColumn>
    </Item>
  )
}, (prevProps, nextProps) => {
  return prevProps.move.name !== nextProps.move.name
})

export default MoveItem