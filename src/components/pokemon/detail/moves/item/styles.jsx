import styled from 'styled-components'
import { Label } from '@micro-ui/components/label'

export const Item = styled.div`
  width: 100%;
  display: grid;
`

export const MoveType = styled.div`
  position: relative;
  font-size: 12px;
`

export const MoveName = styled.label`
  margin-bottom: 20px;
  text-transform: capitalize;
  ${Label}
`

export const Level = styled.label`
  text-align: center;

  ${Label}
`

export const Column = styled.div`
  position: relative;
  padding: 5px 10px;
  font-size: 14px;

  &:last-child {
    border-right: none;
  }
`

export const RangeColumn = styled.div`
  width: 100%;
  max-width: 300px;
  position: relative;
  padding: 5px 10px;
  font-size: 14px;
`

export const Title = styled.h2`
  margin: 0 0 10px;
`

export const Range = styled.div`
  width: 100%;
  position: relative;
  grid-columns-template: repeat(2,1fr);
  display: flex;
  align-items: center;
`

export const RangeSlider = styled.div`
  width: 100%;
  height: 8px;
  background-color: #dadade;
  flex-grow: 2;
  overflow: hidden;
  border-radius: 30px;
  position: relative;

  &:after {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    width: ${props => props.width}%;
    height: 100%;
    display: block;
    background-color: #f387ff;
    overflow: hidden;
    border-radius: 30px;
  }

  &:nth-child(2) {
    &:after {
      background-color: #f060b9;
    }
  }

  &:nth-child(3) {
    &:after {
      background-color: #8cb9bf;
    }
  }
`