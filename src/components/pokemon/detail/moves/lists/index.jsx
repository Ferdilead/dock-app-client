import React from 'react'
import MoveItem from '@components/pokemon/detail/moves/item'
import { List } from './styles'

/**
 * List move of pokemon 
 */
const MoveList = ({ moves })  => (
  <List>
    {moves.splice(0, 10).map((item, id) => (
      <MoveItem key={id} {...item} />
    ))}
  </List>
)

export default MoveList