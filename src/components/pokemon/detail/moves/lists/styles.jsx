import styled from 'styled-components'

export const List = styled.div`
  display: grid;
  grid-template-rows: 1fr;
  grid-gap: 10px;
`