import React from 'react'
import styled, { keyframes } from 'styled-components'
import { Column } from '@components/pokemon/detail/about/styles'
import { Label } from '@micro-ui/components/label'

const keyframesSlide = keyframes`
  @keyframes slide {
    from {
      width: 0;
    }

    to {
      width: 100%;
    }
  }
`

const Range = styled.div`
  width: 100%;
  height: 7px;
  position: relative;
  background-color: #dadade;
  border-radius: 20px;
  overflow: hidden;

  &:after {
    content: '';
    width: ${props => props.width}%;
    height: 100%;
    position: absolute;
    left: 0;
    top: 0;
    display: block;
    border-radius: 20px;
    overflow: hidden;
    background-color: tomato;
    animation: .5s ${keyframesSlide} ease-out;
  }
`

const ColumnFlex = styled(Column)`
  display: flex;
  align-items: center;
`
/**
 * Stat of item
 * 
 * @param {Object} stat 
 */
const StatItem = ({ base_stat, stat }) => (
  <React.Fragment>
    <ColumnFlex>
      <Label width={200}>{stat.name}</Label>
      <Range width={base_stat} />
    </ColumnFlex>
  </React.Fragment>
)

/**
 * Stats of item
 * 
 * @param {Object} Stats 
 */
const Stats = ({ stats }) => (
  <React.Fragment>
    {stats.map((stat, id) => (
      <StatItem key={id} {...stat} />
    ))}
  </React.Fragment>
)

export default Stats