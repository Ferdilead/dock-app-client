import React from 'react'
import { SpinnerContainer, Image } from './styles'
import SpinnerIcon from '@/assets/images/spinner.svg'

/**
 * Spinner components
 */
const Spinner = () => (
  <SpinnerContainer>
    <Image src={SpinnerIcon} alt="Please wait ..."></Image>
  </SpinnerContainer>
)

export default Spinner