import styled from 'styled-components'

export const SpinnerContainer = styled.div`
  width: 100%;
  height: 50px;
  position: relative;
`

export const Image = styled.img`
  position: absolute;
  width: 50px;
  height: auto;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  margin: auto;
`