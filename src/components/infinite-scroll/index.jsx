import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import { debounce } from '@utils/debounce'
import Spinner from './Spinner'

/**
 * Inifinte scroll
 */
const InifiniteScroll = (props) => {
  useEffect(() => {
    window.onscroll = debounce((e) => {
      if ((window.innerHeight + window.scrollY) >= document.body.scrollHeight - 10) {
        props.onEndChange()                            
      }
    }, 300)
  })

  return(
    <React.Fragment>
      {props.children}

      {(props.fallback) && <Spinner />}
    </React.Fragment>
  )
}

InifiniteScroll.propTypes = {
  onEndChange: PropTypes.func
}

InifiniteScroll.defaultProps = {
  onEndChange: () => {}
}

export default InifiniteScroll