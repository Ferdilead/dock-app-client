import React from 'react'
import Spinner from '@/assets/images/spinner.svg'
import styled from 'styled-components'

const LoadingContainer = styled.div`
  width: 100%;
  height: 100%;
  position: fixed;
`

const LoadingSpinner = styled.img`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  margin: auto;
`

const Loading = () => (
  <LoadingContainer>
    <LoadingSpinner img={Spinner} />
  </LoadingContainer>
)

export default Loading