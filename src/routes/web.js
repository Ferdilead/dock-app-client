import React, { lazy, Suspense } from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import { setCurrentUser } from '@store/user/actions'
import { selectCurrentUser } from '@store/user/selector'
import Loading from '@components/loading'

const LoadingPage = () => (
  <React.Fragment>
    <h1>Loading ...</h1>
  </React.Fragment>
)

/**
 * List of routes
 */
const routes = [
  {
    path: '/',
    component: lazy(() => import('@/pages/home')),
    exact: true
  },
  {
    path: '/poke/:pokemonId',
    component: lazy(() => import('@/pages/detail'))
  },
  {
    path: '/my-collection',
    component: lazy(() => import('@/pages/user'))
  }
]

/**
 * Web Common Routes
 * 
 * @param {Object} route 
 */
const CommonRoutes = (route) => (
  <Suspense fallback={false}>
    <Route path={route.path} render={props => ( 
      <route.component {...props} routes={route.routes} />
    )} />
  </Suspense>
)

/**
 * App router
 */
const WebRouter = props => (
  <Switch>
    {routes.map((route, key) => (
      <CommonRoutes key={key} {...route} />
    ))}

    <Route path='/my-collection' render={() => props.currentUser && (<Redirect to='/' />)} />
  </Switch>
)

const mapStateToProps = state => createStructuredSelector({
  currentUser: selectCurrentUser
})

const mapDispacthToProps = dispatch => ({
  setCurrentUser: user => dispatch(setCurrentUser(user))
})

export default connect(mapStateToProps, mapDispacthToProps)(WebRouter)