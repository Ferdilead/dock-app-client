import { put, takeLatest, call, all } from 'redux-saga/effects'
import { GOOGLE_SIGN_IN_START, CHECK_USER_SESSION } from './constant'
import { auth, googleProvider, createUserProfileDocument, getCurrentUser } from '@/firebase/firebase.util'
import { googleSignInFailure, googleSignInSuccess } from './actions' 

/**
 * Get snapsot From User Auth
 * 
 * @param {Object} userAuth
 */
export function* getSnapShotFromUserAuth(userAuth) {
  try {
    const userRef = yield call(createUserProfileDocument, userAuth)
    const userSnapShot = yield userRef.get()

    yield put(googleSignInSuccess({
      id: userSnapShot.id,
      ...userSnapShot.data()
    }))
  } catch(e) {
    yield put(googleSignInFailure(e))
  }
}

/**
 * Sign in with google
 */
export function* signInWithGoogle() {
  try {
    const { user } = yield auth.signInWithPopup(googleProvider)
    
    yield getSnapShotFromUserAuth(user)
  } catch(e) {
    yield put(googleSignInFailure(e))
  }
}

/**
 * Check if user authenticated
 */
export function* isUserAuthenticated() {
  try {
    const userAuth = yield getCurrentUser()

    if(!userAuth) return

    yield getSnapShotFromUserAuth(userAuth)
  } catch(error) {
    yield put(googleSignInFailure(error))
  }
}

/**
 * Check user session
 */
export function* onCheckUserSession() {
  yield takeLatest(CHECK_USER_SESSION, isUserAuthenticated)
}


/**
 * On Google Sign In Start
 */
function* onGoogleSignInStart() {
  yield takeLatest(GOOGLE_SIGN_IN_START, signInWithGoogle)
}

export default function* watchUserSaga() {
  yield all([
    call(onGoogleSignInStart),
    call(isUserAuthenticated)
  ])
}