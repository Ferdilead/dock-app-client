import { 
  SET_CURRENT_USER, 
  GOOGLE_SIGN_IN_START, 
  GOOGLE_SIGN_IN_SUCCESS, 
  GOOGLE_SIGN_IN_FAILURE,
  CHECK_USER_SESSION 
} from './constant'

/**
 * Set curent user
 * 
 * @param {String} user 
 */
export const setCurrentUser = user => ({
  type: SET_CURRENT_USER,
  payload: user
})

/**
 * Google sign in start
 */
export const googleSignInStart = () => ({
  type: GOOGLE_SIGN_IN_START
})

/**
 * Google sign success
 * 
 * @param {Object} user 
 */
export const googleSignInSuccess = user => ({
  type: GOOGLE_SIGN_IN_SUCCESS,
  payload: user
})

/**
 * Google sign in errora
 * 
 * @param {Object} error 
 */
export const googleSignInFailure = error => ({
  type: GOOGLE_SIGN_IN_FAILURE,
  payload: error
})

/**
 * Check user actions
 */
export const checkUserSession = () => ({
  type: CHECK_USER_SESSION
})