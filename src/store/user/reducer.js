import { Map, fromJS } from 'immutable'
import { GOOGLE_SIGN_IN_SUCCESS, GOOGLE_SIGN_IN_FAILURE } from './constant'

const INTIAL_STATE = Map({
  currentUser: Map(), 
  error: Map()
})

/**
 * User reducer
 * 
 * @param {Object} state 
 * @param {Object} action 
 */
export const userReducer = (state = INTIAL_STATE, action) => {
  switch(action.type) {
    case GOOGLE_SIGN_IN_SUCCESS:
      return state.merge({
        currentUser: fromJS(action.payload),
        error: fromJS(Map())
      })
    case GOOGLE_SIGN_IN_FAILURE:
      return state.set('error', fromJS(action.payload))
    default:
      return state
  }
}