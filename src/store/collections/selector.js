import { createSelector } from 'reselect'

export const getCollections = state => state.collections

export const selectCollections = createSelector(
  [getCollections],
  collections => collections
)