import { call, put, all, takeLatest, select } from 'redux-saga/effects'
import { GET_COLLECTIONS_START, POST_COLLECTIONS_START, GET_ALL_COLLECTIONS_START, DELETE_COLLECTIONS_START } from './constant'
import { getAllCollectionsStart, getCollectionsStart, getCollectionsSuccess, getCollectionsFailure, postCollectionsSuccess, postCollectionsFailure, getAllCollectionsSuccess, deleteCollectionsSuccess, deleteCollectionsFailure } from './actions'
import { getPokemonDocument, getCurrentUser, createPokemonDocument } from '@/firebase/firebase.util'
import { getDetailPokemon } from '@store/pokemon-detail/selector'

/**
 * Snapshot collections
 * 
 * @param {Object} userAuth 
 * @param {Object} pokemon 
 */
function* snapShotCollections(pokemon) {
    const userAuth = yield getCurrentUser()
    const collectionsRef = yield call(getPokemonDocument, userAuth, pokemon)
    const collectionsSnapShot = yield collectionsRef.get()

    return collectionsSnapShot
}

/**
 * Get collections of pokemon
 * 
 * @param {Object} payload 
 */
function* getCollection({payload: { userAuth, pokemon }}) {
  try {
    const pokemon = yield select(getDetailPokemon)
    const pokemonJSON = yield pokemon.get('pokemonDetail').toJS() 
    const collectionsSnapShot = yield snapShotCollections(pokemonJSON) 

    yield put(getCollectionsSuccess({
      caught: !collectionsSnapShot.empty,
      lists: collectionsSnapShot.docs.map(doc => doc.data())
    }))
    
  } catch(e) {
    yield put(getCollectionsFailure(e))
  }
}

/**
 * Get all collections
 */
function* getAllCollections() {
  try {
    const collectionsSnapShot = yield snapShotCollections() 

    yield put(getAllCollectionsSuccess({
      caught: !collectionsSnapShot.empty,
      lists: collectionsSnapShot.docs.map(doc => doc.data())
    }))
  } catch(e) {
    yield put(getCollectionsFailure(e))
  }
}

/**
 * Post collections success
 */
function* postCollections({payload: {nickname}}) {
  try {
    const userAuth = yield getCurrentUser()
    const pokemon = yield select(getDetailPokemon)
    const { id, name, sprites, types } = pokemon.get('pokemonDetail').toJS()
    const postCollections = yield call(createPokemonDocument, userAuth, { id, name, sprites, types, nickname })
    
    if(postCollections.id) {
      yield put(postCollectionsSuccess())

      yield put(getCollectionsStart())
    }
  } catch(e) {
    yield put(postCollectionsFailure())
  }
}

/**
 * Delete collections
 */
function* deleteCollection(action) {
  try {
    const collectionsSnapShot = yield snapShotCollections(action.payload)
    
    yield collectionsSnapShot.forEach(doc => {
      doc.ref.delete()
    })

    yield put(deleteCollectionsSuccess())
    yield put(getAllCollectionsStart())
  } catch(e) {
    yield put(deleteCollectionsFailure())
  }
}

/**
 * On post collection start
 */
function* onPostCollectionStart() {
  yield takeLatest(POST_COLLECTIONS_START, postCollections)
}

/**
 * On get pokemon collections start
 */
function* onGetCollectionStart() {
  yield takeLatest(GET_COLLECTIONS_START, getCollection)
}

function* onGetAllCollections() {
  yield takeLatest(GET_ALL_COLLECTIONS_START, getAllCollections)
}

function* onDeleteCollection() {
  yield takeLatest(DELETE_COLLECTIONS_START, deleteCollection)
}

export default function* watchCollectionSaga() {
  yield all([
    call(onGetCollectionStart),
    call(onPostCollectionStart),
    call(onGetAllCollections),
    call(onDeleteCollection)
  ])
}