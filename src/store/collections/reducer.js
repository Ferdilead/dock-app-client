import {
  GET_COLLECTIONS_START,
  GET_COLLECTIONS_SUCCESS,
  GET_COLLECTIONS_FAILURE,
  POST_COLLECTIONS_START,
  POST_COLLECTIONS_SUCCESS,
  POST_COLLECTIONS_FAILURE,
  UPDATE_COLLECTIONS_START,
  UPDATE_COLLECTIONS_SUCCESS,
  UPDATE_COLLECTIONS_FAILURE,
  DELETE_COLLECTIONS_START,
  DELETE_COLLECTIONS_SUCCESS,
  DELETE_COLLECTIONS_FAILURE,
  GET_ALL_COLLECTIONS_START,
  GET_ALL_COLLECTIONS_SUCCESS,
} from './constant'
import { Map, List, fromJS } from 'immutable'

const INITIAL_STATE = Map({
  caught: false,
  lists: List(),
  storage: List()
})

/**
 * My pookemon collection
 * 
 * @param {Object} state 
 * @param {Object} action 
 */
export const collectionsReducer = (state = INITIAL_STATE, action) => {
  switch(action.type) {
    case GET_COLLECTIONS_SUCCESS:
      return state.merge({
        caught: fromJS(action.payload.caught),
        lists: fromJS(action.payload.lists)
      })
    case GET_ALL_COLLECTIONS_SUCCESS:
      return state.set('storage', fromJS(action.payload.lists))
    default:
      return state
  }
}