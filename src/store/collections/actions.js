import {
  GET_ALL_COLLECTIONS_START,
  GET_COLLECTIONS_START,
  GET_COLLECTIONS_SUCCESS,
  GET_COLLECTIONS_FAILURE,
  POST_COLLECTIONS_START,
  POST_COLLECTIONS_SUCCESS,
  POST_COLLECTIONS_FAILURE,
  UPDATE_COLLECTIONS_START,
  UPDATE_COLLECTIONS_SUCCESS,
  UPDATE_COLLECTIONS_FAILURE,
  DELETE_COLLECTIONS_START,
  DELETE_COLLECTIONS_SUCCESS,
  DELETE_COLLECTIONS_FAILURE,
  GET_ALL_COLLECTIONS_SUCCESS,
} from './constant'

/**
 * Start requesy document
 */
export const getCollectionsStart = (userAuth, pokemon) => ({
  type: GET_COLLECTIONS_START,
  payload: {
    userAuth, 
    pokemon
  }
})

/**
 * Get all collection ny user id
 */
export const getAllCollectionsStart = () => ({
  type: GET_ALL_COLLECTIONS_START
})

/**
 * Succe request document
 */
export const getCollectionsSuccess = items => ({
  type: GET_COLLECTIONS_SUCCESS,
  payload: items
})

/**
 * Succe request document
 */
export const getAllCollectionsSuccess = items => ({
  type: GET_ALL_COLLECTIONS_SUCCESS,
  payload: items
})

/**
 * Failure request document
 */
export const getCollectionsFailure = error => ({
  type: GET_COLLECTIONS_FAILURE,
  payload: error
})

/**
 * Start post document
 */
export const postCollectionsStart = (nickname) => ({
  type: POST_COLLECTIONS_START,
  payload: nickname
})

/**
 * Succe post document
 */
export const postCollectionsSuccess = items => ({
  type: POST_COLLECTIONS_SUCCESS,
  payload: items
})

/**
 * Failure postt document
 */
export const postCollectionsFailure = error => ({
  type: POST_COLLECTIONS_FAILURE,
  payload: error
})

/**
 * Start update document
 */
export const updateCollectionsStart = () => ({
  type: UPDATE_COLLECTIONS_START
})

/**
 * Succe update document
 */
export const updateCollectionsSuccess = items => ({
  type: UPDATE_COLLECTIONS_SUCCESS,
  payload: items
})

/**
 * Failure update document
 */
export const updateCollectionsFailure = error => ({
  type: UPDATE_COLLECTIONS_FAILURE,
  payload: error
})

/**
 * Start delete document
 */
export const deleteCollectionsStart = pokemon => ({
  type: DELETE_COLLECTIONS_START,
  payload: pokemon
})

/**
 * Succe delete document
 */
export const deleteCollectionsSuccess = items => ({
  type: DELETE_COLLECTIONS_SUCCESS,
  payload: items
})

/**
 * Failure delete document
 */
export const deleteCollectionsFailure = error => ({
  type: DELETE_COLLECTIONS_FAILURE,
  payload: error
})

