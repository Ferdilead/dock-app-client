import { createStore, applyMiddleware } from 'redux'
import logger from 'redux-logger'
import thunk from 'redux-thunk'
import createSagaMiddleware from 'redux-saga'
import rootReducer from './rootReducer'
import rootSaga from './rootSaga'

// Create saga middleware
const sagaMiddleware = createSagaMiddleware()

// List of middlewares
const middlewares = [sagaMiddleware, logger]

// Create store
const configureStore = createStore(
  rootReducer,
  applyMiddleware(...middlewares)
)

// Run saga
sagaMiddleware.run(rootSaga)

export default configureStore 