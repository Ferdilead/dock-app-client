import { combineReducers } from 'redux'

import { pokemonReducer } from './pokemon/reducer'
import { pokemonDetailReducer } from './pokemon-detail/reducer'
import { userReducer } from './user/reducer'
import { collectionsReducer } from './collections/reducer'

export default combineReducers({
  pokemons: pokemonReducer,
  detail: pokemonDetailReducer,
  user: userReducer,
  collections: collectionsReducer 
})