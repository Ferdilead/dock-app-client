import { createSelector } from 'reselect'

export const getDetailPokemon = state => state.detail

export const selectDetailPokemon = createSelector(
  [getDetailPokemon],
  pokemon => pokemon
)