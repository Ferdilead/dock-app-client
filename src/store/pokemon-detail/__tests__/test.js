import { put, fork } from 'redux-saga/effects'
import { requestStart, requestFailed } from '@store/pokemon-detail/actions'
import { fetchDetailPokemon, cancelRequest, addPokemonItems } from '../saga'
import { getDetailPokemon } from '../selector'

describe('Pokemon detail saga', () => {
  test('Fetch detail pokemon flow', () => {
    const apiUrl = 'https://pokeapi.co/api/v2/pokemon/'
    const params = {action: {payload: apiUrl}}
    const generator = fetchDetailPokemon(params)

    generator.next()
    
    expect(generator.next(getDetailPokemon).value).toEqual(fork(cancelRequest))

    generator.next()

    expect(generator.next(getDetailPokemon).value).toEqual(put(requestStart()))

    generator.next()
    
    expect(generator.next().value).toEqual(fork(addPokemonItems, undefined))
  })

  it('Fetch detail pokemon flow with exception as expected', () => {
    const apiUrl = 'https://pokeapi.co/api/v2/pokemon?offset=1&limit=50'
    const params = {action: {payload: apiUrl}}
    const generator = fetchDetailPokemon(params)

    generator.next()

    expect(generator.next(getDetailPokemon).value).toEqual(fork(cancelRequest))

    generator.next()

    expect(generator.next(getDetailPokemon).value).toEqual(put(requestStart()))
    
    generator.next()

    expect(generator.throw('error').value).toEqual(put(requestFailed()))
  })
})