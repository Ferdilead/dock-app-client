import { call, put, takeLatest, select, cancel, fork, take } from 'redux-saga/effects'
import Url from '@helpers/urls' 
import * as api from '@/api/pokemon'
import { requestStart, requestSuccess, requestFailed } from '@store/pokemon-detail/actions'
import { FETCH_REQUEST } from '@store/pokemon-detail/constant'
import { getDetailPokemon } from './selector'
import { getCollectionsStart } from '@store/collections/actions'
import { requestSpeciesSuccess, requestSpeciesFailed } from './actions'


/**
 * Fetch pokemon
 *
 * @param {Object} action
 */
export function* fetchDetailPokemon(action) {
  const apiUrl = yield Url.API_POKEMON_URL.concat(`/${action.payload}`)
  
  yield fork(cancelRequest)

  yield fork(fetchSpeciesPokemon, action.payload)

  yield put(requestStart())
  
  try {
    const collections = yield call(api.fetchData, apiUrl)

    yield fork(addPokemonItems, collections)
  } catch(e) {
    console.error('FAILED_FETCH_POKEMON', e)
    yield put(requestFailed(e))
  }
}

/**
 * Fetch species pokemon
 */
export function* fetchSpeciesPokemon(pokemonId) {
  const apiUrl = yield Url.API_POKEMON_SPECIES_URL.concat(pokemonId)

  try {
    const collections = yield call(api.fetchData, apiUrl)

    yield put(requestSpeciesSuccess(collections))
  } catch(e) {
    console.error('FAILED_FETCH_POKEMON_SPECIES', e)
    yield put(requestSpeciesFailed())
  }
}

/**
 * Cancel http request
 */
export function* cancelRequest() {
  const pokemons = yield select(getDetailPokemon)

  // Prevent dupplicate request
  if(pokemons.get('isFetching'))
    yield cancel()
}

/**
 * Add pokemon items
 * 
 * @param {Object} collections 
 */
export function* addPokemonItems(collections) {
  yield put(requestSuccess(collections))

  yield put(getCollectionsStart())
}

export default function* pokemonDetailActionWatcher() {
  yield takeLatest(FETCH_REQUEST, fetchDetailPokemon)
}