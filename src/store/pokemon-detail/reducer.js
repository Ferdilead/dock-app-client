import { Map, List, fromJS } from 'immutable'
import { FETCH_REQUEST_START, FETCH_REQUEST_SUCCESS, FETCH_SPECIES_REQUEST_SUCCESS, FETCH_REQUEST_FAILED } from './constant'
import { destructure } from '@utils/destructure'

export const INITIAL_STATE = Map({
  isFetching: false,
  didInvalidate: false,
  metadata: List(),
  pokemonDetail: Map(),
  pokemonSpecies: Map()
})

/**
 * Pokemon detail reducer
 * 
 * @param {Object} state 
 * @param {Object} action 
 */
export const pokemonDetailReducer = (state = INITIAL_STATE, action) => {
  switch(action.type) {
    case FETCH_REQUEST_START:
      return state.set('isFetching', fromJS(true))
    case FETCH_REQUEST_SUCCESS:
      return state.merge({
        isFetching: fromJS(false),
        pokemonDetail: fromJS(action.payload)
      })
      case FETCH_SPECIES_REQUEST_SUCCESS:
          return state.merge({
            // isFetching: fromJS(false),
            pokemonSpecies: fromJS(action.payload)
          })
    case FETCH_REQUEST_FAILED:
      return state.merge({
        isFetching: fromJS(false),
        didInvalidate: fromJS(true)
      })
    default:
      return state
  }
}