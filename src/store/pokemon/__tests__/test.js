import { call, put, select, fork } from 'redux-saga/effects'
import * as api from '@/api/pokemon'
import { requestStart, requestFailed } from '@store/pokemon/actions'
import { fetchPokemon, cancelRequest, addPokemonItems } from '../saga'
import { getPokemons } from '../selector'

describe('Pokemon saga', () => {
  test('Fetch pokemon flow', () => {
    const apiUrl = 'https://pokeapi.co/api/v2/pokemon?offset=1&limit=50'
    const params = {action: {payload: apiUrl}}
    const generator = fetchPokemon(params)

    generator.next()
    generator.next()

    expect(generator.next(getPokemons).value).toEqual(fork(cancelRequest))

    expect(generator.next(getPokemons).value).toEqual(put(requestStart()))
    
    expect(generator.next().value).toEqual(call(api.fetchData, getPokemons))

    expect(generator.next().value).toEqual(fork(addPokemonItems, undefined))
  })

  it('Fetch pokemon flow with exception as expected', () => {
    const apiUrl = 'https://pokeapi.co/api/v2/pokemon?offset=1&limit=50'
    const params = {action: {payload: apiUrl}}
    const generator = fetchPokemon(params)

    generator.next()
    generator.next()

    expect(generator.next(getPokemons).value).toEqual(fork(cancelRequest))

    expect(generator.next(getPokemons).value).toEqual(put(requestStart()))
    
    expect(generator.next().value).toEqual(call(api.fetchData, getPokemons))

    expect(generator.next().value).toEqual(fork(addPokemonItems, undefined))

    expect(generator.throw('error').value).toEqual(put(requestFailed()))
  })
})