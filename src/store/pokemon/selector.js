import { createSelector } from 'reselect'

export const getPokemons = state => state.pokemons

export const selectPokemons = createSelector(
  [getPokemons],
  pokemons => pokemons
)