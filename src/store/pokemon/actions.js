import { FETCH_REQUEST, FETCH_REQUEST_START, FETCH_REQUEST_SUCCESS, FETCH_REQUEST_FAILED } from './constant'

/**
 * Fetch request
 */
export const requestPost = (params) => ({
  type: FETCH_REQUEST,
  payload: params
})

export const requestStart = () => ({
  type: FETCH_REQUEST_START
})

// export const loadMore

/**
 * Fetch request successed
 * @param {Object} response
 */
export const requestSuccess = response => ({
  type: FETCH_REQUEST_SUCCESS,
  payload: response
})

/**
 * Fetch reques failed
 * 
 * @param {Oject} response
 */
export const requestFailed = response => ({
  type: FETCH_REQUEST_FAILED,
  // payload: response
})