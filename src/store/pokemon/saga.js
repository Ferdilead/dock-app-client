import { call, put, takeLatest, select, cancel, fork, take } from 'redux-saga/effects'
import Url from '@helpers/urls' 
import * as api from '@/api/pokemon'
import { requestStart, requestSuccess, requestFailed } from '@store/pokemon/actions'
import { FETCH_REQUEST } from '@store/pokemon/constant'
import { getPokemons } from './selector'

// const getPokemons = state => state.pokemons

/**
 * Fetch pokemon
 *
 * @param {Object} action
 */
export function* fetchPokemon(action) {
  const defaultApiUrl = yield Url.API_POKEMON_URL.concat('?offset=1&limit=20')
  const apiUrl = yield action.payload === undefined ? defaultApiUrl : action.payload
  
  yield fork(cancelRequest)

  yield put(requestStart())
  
  try {
    const collections = yield call(api.fetchData, apiUrl)

    yield fork(addPokemonItems, collections)
  } catch(e) {
    console.error('FAILED_FETCH_POKEMON', e)
    yield put(requestFailed(e))
  }
}

/**
 * Cancel http request
 */
export function* cancelRequest() {
  const pokemons = yield select(getPokemons)

  // Prevent dupplicate request
  if(pokemons.get('isFetching'))
    yield cancel()
}

/**
 * Add or update the pokemon items
 * 
 * @param {Object} collections 
 */
export function* addPokemonItems(collections) {
  const state = yield select(state => state.pokemons)
  const items = state.get('items').toJS()

  // Merge existing array
  collections.results = items.concat(collections.results)
  
  // Set last page
  collections.lastPage = (collections.next === null)

  yield put(requestSuccess(collections))
}

export default function* pokemonActionWatcher() {
  yield takeLatest(FETCH_REQUEST, fetchPokemon)
}