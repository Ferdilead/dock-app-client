import { all } from 'redux-saga/effects'

import pokemonSaga from './pokemon/saga'
import pokemonDetailSaga from './pokemon-detail/saga'
import userSaga from './user/saga'
import collectionsSaga from './collections/saga'

export default function* rootSaga() {
  yield all([
    pokemonSaga(),
    pokemonDetailSaga(),
    userSaga(),
    collectionsSaga()
  ])
}