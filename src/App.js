import React, { Component } from 'react'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import { auth, createUserProfileDocument } from '@/firebase/firebase.util' 
import WebRouter from '@/routes/web'
import Header from '@components/header'
import styled from 'styled-components'
import { setCurrentUser } from '@store/user/actions'
import { selectCurrentUser } from '@store/user/selector'
import { checkUserSession } from '@store/user/actions'
import './App.css'

const MainContent = styled.div`
  max-width: 1120px;
  margin: 0 auto;
  position: relative;
  padding: 10px 0;
`

class App extends Component {
  unsubscribeFromAuth = null

  componentDidMount() {
    const { checkUserSession } = this.props

    checkUserSession()
  }

  componentWillUnmount() {
    this.unsubscribeFromAuth()
  }

  render() {
    return (
      <div className="App">
        <Header />

        <MainContent>
          <WebRouter />
        </MainContent>
      </div>
    )
  }
}

const mapStateToProps = createStructuredSelector({
  currentUser: selectCurrentUser
})

const mapDispacthToProps = dispatch => ({
  setCurrentUser: user => dispatch(setCurrentUser(user)),
  checkUserSession: () => dispatch(checkUserSession())
})

export default connect(mapStateToProps, mapDispacthToProps)(App)
