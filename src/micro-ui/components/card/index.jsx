import styled from 'styled-components'

const Card = styled.div`
  border: 1px solid #dadade;
  box-shadow: 1px 1px 2px #dadade;
  overflow: hidden;
`
export default Card