import styled from 'styled-components'

export const Badge = styled.span`
  background-color: ${({backgroundColor}) => backgroundColor ? backgroundColor : 'tomato'};
  padding: 0 10px;
  border-radius: 5px;
  color: ${({textColor}) => textColor ? textColor : '#ffffff'};
  margin-right: 5px;
  text-transform: lowercase;

  &:last-child {
    margin-right: 0;
  }
`