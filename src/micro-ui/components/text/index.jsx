import styled from 'styled-components'

export const Text = styled.p`
  font-size: 12px;
  margin: 0;
  text-transform: capitalize;
  font-weight: 300;
`

export const TextEllipsis = styled.p`
  text-overflow: ellipsis;
  white-space: nowrap;
  max-width: 165px;
  float: left;
  overflow: hidden;
`