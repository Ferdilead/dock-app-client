import styled from 'styled-components'

export const Label = styled.label`
  width: ${({width}) => width ? width : 100}px;
  text-transform: capitalize;
  font-size: 12px;
  float: left;
`