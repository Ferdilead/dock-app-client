#!/bin/bash

# Rename the environment files
mv .firebaserc.example .firebaserc
mv .env.example .env

# Set Firebase Hosting Token, Project ID
# cat .env
# export FIREBASE_HOSTING_TOKEN=$FIREBASE_TOKEN
# echo $FIREBASE_HOSTING_TOKEN
sed -i "s/\[FIREBASE\-\PROJECT\-\ID\]/$FIREBASE_PROJECT_ID/g" .firebaserc
